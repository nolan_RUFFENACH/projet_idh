#!/usr/bin/env python

# pour lancer : ./blastset.neo4j.py -q set.01.txt -t GOTerm --species 511145 -c -v

#####################################################################################################
#       Ce script permet l'enrichissement du jeu de données à partir d'un ensemble requete.         #
# Il permet aussi la comparaison des 4 méthodes à étudier via le calcul de la moyenne des p-valeurs #
#                   Ou bien encore via la création de plot en 3D.                                   #
#####################################################################################################

import argparse
from os.path import isfile
from scipy.stats import binom, hypergeom, chi2_contingency
import numpy as np
from py2neo import *
from py2neo import Graph
from statistics import *
from matplotlib import *
import matplotlib.pyplot as plt

# SCRIPT PARAMETERS
parser = argparse.ArgumentParser(description='Search enriched terms/categories in the provided (gene) set')
parser.add_argument('-q', '--query', required=True, help='Query set.')
parser.add_argument('-t', '--sets', required=True, help='Target sets node type.')
parser.add_argument('-s', '--species', required=True, type=int, help='Taxon id')
parser.add_argument('-a', '--alpha', required=False, type=float, default=0.05, help='Significance threshold.')
parser.add_argument('-c', '--adjust', required=False, action="store_true", help='Adjust for multiple testing (FDR).')
# parser.add_argument('-m', '--measure', required=False, default='binomial', help='Dissimilarity index: binomial (default), hypergeometric, chi2 or coverage. chi2 and coverage are NOT YET IMPLEMENTED')
parser.add_argument('-l', '--limit', required=False, type=int, default=0, help='Maximum number of results to report.')
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Talk a lot.')
parser.add_argument('-m', '--measure', required=False, help='Dissimilarity index: binomial (default), hypergeometric, chi2 or coverage.')
param = parser.parse_args()

# LOAD QUERY
text = param.query
query = set()

if isfile(text):
    with open(text) as f:
        #print(f)
        content = ' '.join(f.read().split('\n')).split()
        query |= set(content)
else:  # parse string
    query |= set(text.split())

#if param.verbose:
    #print(f'query set: {query}')

# CONNECT TO NEO4J

graph = Graph("bolt://localhost:7687", auth=("neo4j", "Marseille09"))
nodes = NodeMatcher(graph)

# COMPUTE POPULATION SIZE

population_size = nodes.match('Gene', taxon_id=param.species).count()

# RETRIEVE TARGET SETS FOR QUERY
#path = '[:is_a|part_of|annotates*]' if param.sets == 'GOTerm' else ''
#cypher = f"MATCH (t:{param.sets})-{path}->(n:Gene {{taxon_id:{param.species} }}) WHERE n.id IN ['" + "', '".join(
#    query) + "'] RETURN DISTINCT t"
#if param.verbose:
#    print(cypher)
#sets = graph.run(cypher)


# EVALUATE SETS

def compar_measure(measure,sets,path):
    results = []
    query_size = len(query)
    print("la taille de la query", query_size)
    for s in sets:  # _ids:
        sid = s['t']['id']
        cypher = "MATCH (t:{})-{}->(n:Gene {{taxon_id:{}}}) WHERE t.id='{}' RETURN DISTINCT n.id".format(param.sets, path,
                                                                                                 param.species, sid)
        #if param.verbose:
            #print(cypher)
        table = graph.run(cypher).to_table()
        elements = set(map(lambda x: x[0], table))
        #elements = set([table[j][0] for j in range(len(table))])
        common_elements = elements.intersection(query)
        if len(common_elements) < 2:
            next
        if measure == 'coverage':
            pvalue = 1 - ((len(common_elements) / query_size) * (len(common_elements) / len(elements)))

        elif measure == 'binomial':  # binom.cdf(>=success, attempts, proba)

            pvalue = binom.cdf(query_size - len(common_elements), query_size,
                               1 - float(len(elements)) / population_size)
        elif measure == 'hypergeometrique':

            pvalue = hypergeom.sf(len(common_elements) + 1, population_size, len(elements), query_size)

        elif measure == 'chi2':
            # Construction du tableau de contingence
            ContingenceTable = np.array([[len(common_elements), query_size - len(common_elements), query_size],
                                         [len(elements) - len(common_elements),
                                          population_size - query_size - len(elements) + len(common_elements),
                                          population_size - query_size],
                                         [len(elements), population_size - len(elements), population_size]])

            #print("##############################################################################################################################################################",
                  #ContingenceTable)

            chi2, pvalue, degrees, expected = chi2_contingency(ContingenceTable)
        r = {'id': sid, 'desc': s['t']['desc'], 'common.n': len(common_elements), 'target.n': len(elements),
             'p-value': pvalue, 'elements.target': elements, 'elements.common': common_elements}
        results.append(r)

    #if param.verbose:
        #print(results)

    return results


# PRINT SIGNIFICANT RESULTS

for m in ["coverage", "binomial", "chi2", "hypergeometrique"]:
    path = '[:is_a|part_of|annotates*]' if param.sets == 'GOTerm' else ''
    cypher = f"MATCH (t:{param.sets})-{path}->(n:Gene {{taxon_id:{param.species} }}) WHERE n.id in ['" + "', '".join(
        query) + "'] RETURN DISTINCT t"
    #if param.verbose:
        #print(cypher)
    sets = graph.run(cypher)

    print("La loi ",m," est utilisée: ")
    results = compar_measure(m,sets,path)

    #print(results)

    results.sort(key=lambda an_item: an_item['p-value'])
    i = 1
    compteur = 0
    pvalmin = None
    pvalmax = None
    liste_pvaleur = []

    nom_fichier = str(m) + ".txt"
    if os.path.exists(nom_fichier):
        os.remove(nom_fichier)  #supprimer si fichier existe deja
    f = open(nom_fichier, 'w')
    for r in results:
        if m!='coverage':
            # FDR
            if param.adjust and r['p-value'] > param.alpha * i / len(results):
                break
            # limited output
            if param.limit > 0 and i > param.limit:
                break
            # alpha threshold
            elif r['p-value'] > param.alpha:
                break
        # OUTPUT
        if param.measure != None and param.measure == m:
            pval = "{:.4f}".format(r['p-value']) if r['p-value'] > 0.01 else "{:.2e}".format(r['p-value'])
            print("{}\t{}\t{}/{}\t{}\t{}".format(r['id'], pval, r['common.n'], r['target.n'], r['desc'],
                                                 ', '.join(r['elements.common'])))
        f.write(r['id']+" "+str(r['p-value']))
        f.write('\n')

        # Methode comparatives :
        compteur += 1
        if pvalmin == None:
            pvalmin = r['p-value']
        else:
            if pvalmin > r['p-value']:
                pvalmin = r['p-value']
        if pvalmax == None:
            pvalmax = r['p-value']
        else:
            if pvalmax < r['p-value']:
                pvalmax = r['p-value']
        liste_pvaleur.append(r['p-value'])

        i += 1
    if pvalmax==None and pvalmin==None:
        print("Pour la méthode {}, nous avons trouvé {} significatifs. Les pvaleur max et min sont nulles".format(m, compteur))
        print("\n")
    else:
        ecart_type = pstdev(liste_pvaleur)
        moyenne = mean(liste_pvaleur)
        print("Pour la méthode {}, nous avons trouvé {} significatifs. La pvaleur max est de {} tandis que la pvaleur min est de {}.".format(m, compteur, pvalmax, pvalmin),
              "\n","la moyenne est de {} et l'écart-type est de {}".format(moyenne, ecart_type))
        print("\n")
    f.close()

def Comparaison_faux_donnee(measure, Q, T, C,G=150):
    if measure == 'binomial':
        pval = binom.cdf(Q - C, Q, 1 - T / G)
    elif measure == 'coverage':
        pval = 1 - (C / Q) * (C / T)
    elif measure == 'hypergeometric':
        pval = hypergeom.sf(C - 1, G, T, Q)
    elif measure == 'chi2':
        contingence = np.array([[C, (Q - C),Q],
                                [(T - C), (G - Q - T) + C,G-Q],[T,G-T,G]])
        pval = chi2_contingency(contingence)[1]  #prend que la pval
    return pval

def Creation_liste_valeur(measure):
    liste_Q = []
    liste_C = []
    liste_T = []
    liste_pval = []
    for T in range(1, 75):
        pval_temp = []
        for Q in range(1, 75):
            for C in range(1, min(Q,T)):
                liste_T.append(T)
                liste_Q.append(Q)
                liste_C.append(C)
                pval = Comparaison_faux_donnee(measure, Q, T, C)
                liste_pval.append(pval)
    return (liste_T, liste_Q, liste_C, liste_pval)


Q_binomial, T_binomial, C_binomial, pval_binomial = Creation_liste_valeur('binomial')
Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric = Creation_liste_valeur('hypergeometric')
Q_chi2, T_chi2, C_chi2, pval_chi2 = Creation_liste_valeur('chi2')
Q_coverage, T_coverage, C_coverage, pval_coverage = Creation_liste_valeur('coverage')

# Fonction permettant la visualisation
def Plot3D(measure,liste_Q, liste_T, liste_C, liste_pvaleur):
   fig = plt.figure()
   ax = fig.gca(projection='3d')
   surface = ax.scatter(liste_Q, liste_T, liste_C, zdir='z', s=30, c=liste_pvaleur, depthshade=True)
   ax.set_xlabel('Q')
   ax.set_ylabel('T')
   ax.set_zlabel('C');
   fig.colorbar(surface)
   title = ax.set_title("Méthode: "+str(measure)+" \n P-value en fonction de différents valeurs de Q, T and C")
   title.set_y(1.01)
   # plt.show()
   nom_fichier = "plot-3d_" + str(measure) + ".png"
   if os.path.exists(nom_fichier):
       os.remove(nom_fichier)  # supprimer si fichier existe deja
   fig.savefig(nom_fichier)

   ecart_type = pstdev(liste_pvaleur)
   moyenne = mean(liste_pvaleur)
   print("L'ecart type est de {} et la moyenne est de {}".format(ecart_type, moyenne))

Plot3D("binomial",Q_binomial, T_binomial, C_binomial, pval_binomial)
Plot3D("hypergeometric",Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric)
Plot3D("chi2",Q_chi2, T_chi2, C_chi2, pval_chi2)
Plot3D("coverage",Q_coverage, T_coverage, C_coverage, pval_coverage)