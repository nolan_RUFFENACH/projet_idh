# Projet_IDH

Gitlab contenant l'ensemble des ressources demandées pour finaliser le projet IDH.

Chaque dossier comporte des fichiers non vides.

- Le dossier images contient les résultats obtenus sur R ou python.

- Le dossier data contient les 2 sets de données ainsi que le fichier uniprot à utiliser pour la fin du script R.

- Le dossier script contient 2 scripts (un sur python et un sur R) exécutables.

- Pour finir, le dossier dump_file contient le fichier .dump permettant de télécharger directement la base de données sur NEO4j.
